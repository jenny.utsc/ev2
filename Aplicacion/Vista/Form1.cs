﻿using Aplicacion.Datos;
using Aplicacion.modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aplicacion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            llenarGrid();
        }

       
        private void button1_Click(object sender, EventArgs e)
        {
            if (txtDocumento.Text.Trim() == "")
            {
                MessageBox.Show("Debe ingresar un documento valido");
            }
            else if (txtNombres.Text.Trim().Length < 5)
            {
                MessageBox.Show("Debe ingresar un nombre mas largo");
            }
            else
            {
                try
                {
                    Empleado em = new Empleado();
                    em.Apellidos = txtApellidos.Text.Trim().ToUpper();
                    em.Nombres = txtNombres.Text.Trim().ToUpper();
                    em.Direccion = txtDireccion.Text.Trim().ToUpper();
                    em.Documento = txtDocumento.Text.Trim().ToUpper();
                    em.Edad = Convert.ToInt32(txtEdad.Text.Trim());
                    em.Fecha_nacimiento = txtFechaNacimiento.Value.Year + "-" + txtFechaNacimiento.Value.Month + "-" + txtFechaNacimiento.Value.Day;

                    if (EmpleadoCAD.guardar(em))
                    {
                        llenarGrid();
                        limpiarcampos();
                        
                        MessageBox.Show("Empleado Guardado correctamente");
                    }
                    else
                    {
                        MessageBox.Show("Ya existe otro empleado con el mismo documento");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void limpiarcampos()
        {
            txtApellidos.Text = "";
            txtNombres.Text = "";
            txtDireccion.Text = "";
            txtDocumento.Text = "";
            txtEdad.Text = "";
            

        }



        private void llenarGrid()
        {
            DataTable datos=EmpleadoCAD.listar();
            if(datos==null)
            {
                MessageBox.Show("No se logro acceder a los datos");
            }
            else
            {
                dgLista.DataSource = datos.DefaultView;
            }
        }

        bool consultado = false;

        private void button2_Click(object sender, EventArgs e)
        {

            if (txtDocumento.Text.Trim() == "")
            {
                MessageBox.Show("Debe ingresar un documento");
            }
            else
            {
                Empleado em = EmpleadoCAD.consultar(txtDocumento.Text.Trim());
                if (em == null)
                {
                    MessageBox.Show("No existe el empleado con documento" + txtDocumento.Text);
                    limpiarcampos();
                    consultado = false;
                }
                else
                {
                    txtApellidos.Text = em.Apellidos;
                    txtNombres.Text = em.Nombres;
                    txtDireccion.Text = em.Direccion;
                    txtDocumento.Text = em.Documento;
                    txtEdad.Text = em.Edad.ToString();
                    txtFechaNacimiento.Text = em.Fecha_nacimiento;
                    consultado = true;


                }
            }          

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (consultado==false)
            {
                MessageBox.Show("Debe consultar el empleado");
            }
            else if(txtDocumento.Text.Trim()=="")
            {
                MessageBox.Show("Debe ingresar un documento valido");
            }
            else if (txtNombres.Text.Trim().Length < 5)
            {
                MessageBox.Show("Debe ingresar un nombre mas largo");
            }
            else
            {
                try
                {
                    Empleado em = new Empleado();
                    em.Apellidos = txtApellidos.Text.Trim().ToUpper();
                    em.Nombres = txtNombres.Text.Trim().ToUpper();
                    em.Direccion = txtDireccion.Text.Trim().ToUpper();
                    em.Documento = txtDocumento.Text.Trim().ToUpper();
                    em.Edad = Convert.ToInt32(txtEdad.Text.Trim());
                    em.Fecha_nacimiento = txtFechaNacimiento.Value.Year + "-" + txtFechaNacimiento.Value.Month + "-" + txtFechaNacimiento.Value.Day;

                    if (EmpleadoCAD.actualizar(em))
                    {
                        llenarGrid();
                        limpiarcampos();
                        MessageBox.Show("Empleado actualizado correctamente");
                        consultado = false;
                    }
                    else
                    {
                        MessageBox.Show("No se actualizo");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (consultado == false)
            {
                MessageBox.Show("Debe consultar el empleado");
            }
            else if (txtDocumento.Text.Trim() == "")
            {
                MessageBox.Show("Debe ingresar un documento valido");
            }
            else if (txtNombres.Text.Trim().Length < 5)
            {
                MessageBox.Show("Debe ingresar un nombre mas largo");
            }
            else
            {
                try
                {
                    

                    if (EmpleadoCAD.eliminar(txtDocumento.Text.Trim()))
                    {
                        llenarGrid();
                        limpiarcampos();
                        MessageBox.Show("Empleado eliminar correctamente");
                        consultado = false;
                    }
                    else
                    {
                        MessageBox.Show("No se elimino");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }
    }
    
}
