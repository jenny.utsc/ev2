﻿using Aplicacion.modelo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aplicacion.Datos
{
    class EmpleadoCAD
    {
        public static bool guardar(Empleado e)
        {
            try
            {
                Conexion con = new Conexion();
                string sql = "INSERT INTO empleados VALUES('"+e.Documento+"','"+e.Nombres+"','"+e.Apellidos+"','"+e.Edad+"','"+e.Direccion+"','"+e.Fecha_nacimiento+"')";
                SqlCommand comando = new SqlCommand(sql,con.conectar());
                int cantidad = comando.ExecuteNonQuery();
                if (cantidad == 1)
                {
                    return true;

                }
                else return false;

                con.desconectar();
      

            }catch(Exception ex)
            {
                return false;
            }
        }


        public static DataTable listar()
        {
            try
            {
                Conexion con = new Conexion();
                string sql = "SELECT * FROM empleados;";
                SqlCommand comando = new SqlCommand(sql, con.conectar());
                SqlDataReader dr =comando.ExecuteReader(CommandBehavior.CloseConnection);
                DataTable dt = new DataTable();
                dt.Load(dr);
                con.desconectar();
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static Empleado consultar(string documento)
        {
            try
            {
                Conexion con = new Conexion();
                string sql = "SELECT * FROM empleados WHERE documento='"+documento+"';";
                SqlCommand comando = new SqlCommand(sql, con.conectar());
                SqlDataReader dr = comando.ExecuteReader();

                Empleado em = new Empleado();
                if(dr.Read())
                {
                    em.Documento = dr["documento"].ToString();
                    em.Nombres = dr["nombres"].ToString();
                    em.Apellidos = dr["apellidos"].ToString();
                    em.Edad = Convert.ToInt32(dr["edad"].ToString());
                    em.Direccion = dr["direccion"].ToString();
                    em.Fecha_nacimiento = dr["fecha_nacimiento"].ToString();
                    return em;
                }
                else
                {
                    con.desconectar();
                    return null;
                }
                
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static bool actualizar(Empleado e)
        {
            try
            {
                Conexion con = new Conexion();
                string sql = "UPDATE empleados SET nombres='" + e.Nombres + "',apellidos='" + e.Apellidos + "',edad='" + e.Edad + "',direccion='" + e.Direccion + "',fecha_nacimiento='" + e.Fecha_nacimiento + "'where documento='" + e.Documento+"'";
                SqlCommand comando = new SqlCommand(sql, con.conectar());
                int cantidad = comando.ExecuteNonQuery();
                if (cantidad == 1)
                {
                    con.desconectar();
                    return true;

                }
                else
                {
                    con.desconectar();
                    return false;
                }
                

            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static bool eliminar(String documento)
        {
            try
            {
                Conexion con = new Conexion();
                string sql = "DELETE FROM empleados WHERE documento='"+ documento+"'";
                SqlCommand comando = new SqlCommand(sql, con.conectar());
                int cantidad = comando.ExecuteNonQuery();
                if (cantidad == 1)
                {
                    con.desconectar();
                    return true;

                }
                else
                {
                    con.desconectar();
                    return false;
                }


            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
