﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Aplicacion.Datos
{
    class Conexion
    {
    
        SqlConnection con;
        public Conexion()
        {
            con =new SqlConnection("Server=SLMX8415PC9\\SQLEXPRESS;Database=proyecto;integrated security=true");

        }

        public SqlConnection conectar()
        {
            try
            {
                con.Open();
                return con;
            }catch(Exception e)
            {
                return null;
            }

            
        }

        public bool desconectar ()
        {
            try
            {
                con.Close();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
         
    }
}
